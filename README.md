[!Live Priview](http://sunecom.codingraja.com)
# Sun eCommerce Web Project

This project deals with developing an e-commerce website for selling products online. It provides the user with a catalog of different products available for purchase in the store. In order to facilitate online purchase a shopping cart is provided to the user. The system is implemented using a 3-tier approach, with a presentation tier, service tier and persistent tier.

## Project Technical Background

* JDK-1.8, JDBC, Servlet-3.1, JSP-2.3.1 and JSTL-1.2, 
* HTML5, CSS3, Bootstrap3, JQuery, Ajax, JSON
* Embedded Tomcat 8.5.0
* MySQL (5.7)
* Eclipse IDE
* Maven 3.0+
* Git

# Getting Started with Project Setup

Follow the below steps and complete your project setup

## Install JDK
If you have already installed the JDK than you can skip this step.
To install the jdk [Click Here](http://codingraja.com/articles/core-java/introduction/jdk-installation).

## Install Maven
If you have already installed the Maven than you can skip this step.
To install the Maven [Click Here](http://codingraja.com/articles/maven).

## Install GIT Base
If you have already installed the GIT Base than you can skip this step.
To install the GIT Base [Click Here](http://codingraja.com/articles/git).

## Generate SSH Key
To generate SSH Key open your GIT Base and execute below command and then press enter.
Do not enter any values, use the default properties only
```
$ ssh-keygen
```
To know more [Click Here](http://codingraja.com/articles/git).

## Add SSH Key with GIT Repository
Login to your repository account-
You can add a deploy key in the project settings under the section 'Repository'. Specify a title for the new deploy key and paste a public SSH key. After this, the machine that uses the corresponding private SSH key has read-only or read-write (if enabled) access to the project.

## Clone Project in your local machine

```
$ git clone git@gitlab.com:codingraja-jdbc-servlet-jsp-project-11-11-2017/sunecom.git
```
## Run your Project

```
$ ./mvnw install
```

```
$ ./mvnw
```

To clean 

```
$ /mvnw clean
```
That�s it. Your project should start up on port 8080. You can see the landingPage at http://localhost:8080
