package com.codingraja.repository;

import com.codingraja.domain.User;

public interface UserRepository {
	public User findByUsername(String username);
}
